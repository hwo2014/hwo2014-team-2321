import json
from collections import namedtuple


def _json_object_hook(d): return namedtuple('X', d.keys())(*d.values())
def json2obj(data): return json.loads(data, object_hook=_json_object_hook)

class InitDataWrapper(object):
	def __init__(self, data):
		self.data = data

	def pieces(self):
		return self.data['race']['track']['pieces']


class TurnDataWrapper(object):
	def __init__(self, data):
		self.players = [json2obj(json.dumps(x)) for x in data]

