
# import pygame

class Visualizer(object):
    def __init__(self, data):
        pygame.init()

        self.screen = pygame.display.set_mode([1280, 800])
        self.font = pygame.font.SysFont("None", 16)

        self.prev_pos = 0

    def draw_tick(data):
        self.screen.fill((0, 0, 0))
