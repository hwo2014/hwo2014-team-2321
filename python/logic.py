
def linear(value, L, R, low, high):
    if value < L: value = L
    if value > R: value = R
    return low + float(high - low) * (value - L) / float(R - L)

class Logic(object):
    def __init__(self, init_data):
        self.pieces = init_data.pieces()
        self.n = len(self.pieces)
        self.prev_pos = 0
        self.prev_angle = 0

    def make_decision(self, turn_data, response):
        for pl in turn_data.players:
            if pl.id.name == "RomkaTeam":
                piece_index = pl.piecePosition.pieceIndex
                speed = pl.piecePosition.inPieceDistance - self.prev_pos
                angle_diff = abs(pl.angle) - self.prev_angle
                self.prev_angle = abs(pl.angle)
                self.prev_pos = pl.piecePosition.inPieceDistance

                if self.is_straight(piece_index):
                    straight_len = self.get_straight_len(pl)
                    throttle = linear(straight_len, 0, 100, 0.4, 1)
                    speed_lim = max(12.5 - speed, 0) / 6.4
                    # print "Straight: {0}, speed is {3}, throttle wants to be {1}, speed_lim is {2}".format(straight_len, throttle, speed_lim, speed)
                    if throttle > speed_lim and straight_len < 200:
                        throttle = speed_lim

                    response.throttle(throttle)
                else:
                    throttle = linear(abs(pl.angle), -15, 30, 1, 0.64) / linear(angle_diff, 0.1, 7, 0.7, 10.5)
                    if throttle > 1:
                        throttle = 1
                    response.throttle(throttle)

    def is_straight(self, i):
        return 'length' in self.pieces[i]

    def get_straight_len(self, pl):
        res = 0
        for x in xrange(self.n):
            i = (pl.piecePosition.pieceIndex + x) % self.n
            if not self.is_straight(i):
                return res

            if x == 0:
                res += self.pieces[i]['length'] - pl.piecePosition.inPieceDistance
            else:
                res += self.pieces[i]['length']
